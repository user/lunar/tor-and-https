/* “Tor and HTTPS”
 * Copyright © 2014 Lunar <lunar@torproject.org>
 *           © 2014 The Tor Project, Inc.
 * Licensed under CC BY 3.0
 */

var page = require('webpage').create();
var system = require('system');
var directory;
var svgContent;
var patched = false;
var snapshotIndex = 0;

page.onConsoleMessage = function(msg){
    console.log(msg);
};

page.onResourceReceived = function (res) {
    if (res.stage == 'end') {
        console.log('received: ' + res.url);
    }
};

function raster() {
    var path = directory + "/tor-and-https-" + snapshotIndex + ".png"
    console.log("Saving " + path);
    page.render(path);
    snapshotIndex++;
}

/* Work around a weird bug in PhantomJS regardig font-rendering:
 * we need to define a font with a font-weight of normal, otherwise,
 * we don't get the proper font used.
 */
function patchFontFace(content) {
    return content.replace(/<\/style>/, "\n" +
            "@font-face {\n" +
            "  font-family: 'Open Sans';\n" +
            "  font-style: normal;\n" +
            "  font-weight: normal;\n" +
            "  src: url(OpenSans-Bold.ttf) format('truetype');\n" +
            "}\n" +
            "</style>");
}

page.onLoadFinished = function (status) {
    if (status !== 'success') {
        console.log('Loading SVG failed.');
        phantom.exit();
    }
    page.evaluate(function() {
        var svgObject = document.getElementsByTagName("svg")[0];
        var backgroundObject = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        backgroundObject.setAttribute("style", "fill: white");
        backgroundObject.setAttribute("x", 0);
        backgroundObject.setAttribute("y", 0);
        backgroundObject.setAttribute("width", svgObject.getAttribute("width"));
        backgroundObject.setAttribute("height", svgObject.getAttribute("height"));
        svgObject.insertBefore(backgroundObject, document.getElementsByTagName("defs")[0].nextSibling);
    });
    window.setTimeout(function () {
            page.evaluate(function() {
                disableTor();
                disableHTTPS();
            });
            raster();
            page.evaluate(function() {
                disableTor();
                enableHTTPS();
            });
            raster();
            page.evaluate(function() {
                enableTor();
                disableHTTPS();
            });
            raster();
            page.evaluate(function() {
                enableTor();
                enableHTTPS();
            });
            raster();
            phantom.exit();
        }, 200);
}

function main() {
    if (system.args.length < 2) {
        console.log('Usage: rasterize.js directory');
        phantom.exit(1);
    }

    directory = system.args[1];
    page.viewportSize = { width: 990, height: 765 };
    page.open(directory + "/tor-and-https-phantomjs.svg");
}

main();
