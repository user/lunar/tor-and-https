msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-07-17 14:23+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. (itstool) path: C/tor-and-https.svg/svg@direction
#. (itstool) comment: C/tor-and-https.svg/svg@direction
#. Specify "ltr" for left-to-right languages or "rtl" for right-to-left
#. languages (e.g. Arabic or Hebrew).
#: C/tor-and-https.svg:3
msgid "ltr"
msgstr ""

#. (itstool) path: svg/title
#: C/tor-and-https.svg:14
#, no-wrap
msgid ""
"Tor and HTTPS"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 7em max. Seven times the capital letter "M".
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Site.com"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 7em max. Seven times the capital letter "M".
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"user / pw"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 7em max. Seven times the capital letter "M".
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"data"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 7em max. Seven times the capital letter "M".
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"location"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 3em max.
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"WiFi"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 4em max.
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"ISP"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 8em is ok, 9em is max.
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Hacker"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 8em is ok, 9em is max.
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Lawyer"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 8em is ok, 9em is max.
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Sysadmin"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 8em is ok, 9em is max.
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Police"
msgstr ""

#. (itstool) path: defs/text
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"NSA"
msgstr ""

#. (itstool) path: defs/text
#. Keep it short: 8em is ok, 9em is max.
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Tor relay"
msgstr ""

#. (itstool) path: defs/text
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Key"
msgstr ""

#. (itstool) path: defs/text
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Internet connection"
msgstr ""

#. (itstool) path: defs/text
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Eavesdropping"
msgstr ""

#. (itstool) path: defs/text
#: C/tor-and-https.svg:363
#, no-wrap
msgid ""
"Data sharing"
msgstr ""

